LTXMK = latexmk -xelatex -pdf -shell-escape -file-line-error --interaction=nonstopmode

all:
	$(LTXMK) regles.tex

clean:
	latexmk -c regles.tex
	rm -f *~
